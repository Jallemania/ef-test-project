﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PostGradEF.Migrations
{
    public partial class updatedeanfacultycol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Faculty",
                table: "Deans",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Faculty",
                table: "Deans");
        }
    }
}
