﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess
{
    public class PostGradDbContext : DbContext
    {
        public DbSet<Professor> ?Professors { get; set; }
        public DbSet<Student> ?Students { get; set; }
        public DbSet<Dean> ?Deans { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionStringBuilder.GetConnectionString());
        }
    }
}
