﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess
{
    public class ConnectionStringBuilder
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new();

            builder.DataSource = "N-SE-01-3674";
            builder.InitialCatalog = "PostGradEfTestDb";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;

            return builder.ConnectionString;
        }

        public ConnectionStringBuilder()
        {

        }
    }
}
