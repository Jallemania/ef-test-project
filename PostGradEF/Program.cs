﻿using PostGradEF.DataAccess;
using PostGradEF.Models;

Console.WriteLine("Hello, World!");

using(PostGradDbContext db = new())
{
    Professor professor = new() { FirstName="Ergin", LastName="Arnautovic",  Phone="083255746" };

    db.Professors.Add(professor);

    if(db.SaveChanges() > 0)
    {
        Console.WriteLine($"{professor.FirstName} was added to the database");
    }
    else
    {
        Console.WriteLine("Update failed...");
    }
}
