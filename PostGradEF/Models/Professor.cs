﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public class Professor
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Phone { get; set; }
    }
}
