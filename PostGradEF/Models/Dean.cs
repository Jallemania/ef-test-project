﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public partial class Dean
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Faculty { get; set; }
        public string? Phone { get; set; }
        public string? Office { get; set; }
    }
}
