﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public partial class Student
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
    }
}
